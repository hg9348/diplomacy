#!/usr/bin/env python3

# ------------------------------
# projects/diplomacy/Diplomacy.py
# Copyright (C)
# Glenn P. Downing
# ------------------------------

# -------
# imports
# -------

import sys

from Diplomacy import diplomacy_solve

# ----
# main
# ----

if __name__ == "__main__":
    diplomacy_solve(sys.stdin, sys.stdout)