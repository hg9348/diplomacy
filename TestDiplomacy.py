#!/usr/bin/env python3

# -------------------------------
# projects/diplomacy/TestDiplomacy.py
# -------------------------------

# https://docs.python.org/3.6/reference/simple_stmts.html#grammar-token-assert_stmt

# -------
# imports
# -------

from io import StringIO
from unittest import main, TestCase

from Diplomacy import diplomacy_read, diplomacy_eval, diplomacy_print, diplomacy_solve, army, world

# -----------
# TestDiplomacy
# -----------

class TestDiplomacy (TestCase):

    # ----
    # read
    # ----
    
    """
    these unit tests test the 
    read function in Diplomacy.py
    
    input: an initial input string
           that includes the army name,
           army location, army action,
           and army target
    output: strings that are split
            based on their element 
            in the string
    """

    def test_read_1(self):
        s = "A Madrid Hold\n"
        i, j, k, l = diplomacy_read(s)
        self.assertEqual(i, "A")
        self.assertEqual(j, "Madrid")
        self.assertEqual(k, "Hold")
        self.assertEqual(l, "Madrid")
        
    def test_read_2(self):
        s = "C Austin Hold\n"
        i, j, k, l = diplomacy_read(s)
        self.assertEqual(i, "C")
        self.assertEqual(j, "Austin")
        self.assertEqual(k, "Hold")
        self.assertEqual(l, "Austin")
        
    def test_read_3(self):
        s = "B Barcelona Move Madrid\n"
        i, j, k, l = diplomacy_read(s)
        self.assertEqual(i, "B")
        self.assertEqual(j, "Barcelona")
        self.assertEqual(k, "Move")
        self.assertEqual(l, "Madrid")
        
    def test_read_4(self):
        s = "D Austin Move London\n"
        i, j, k, l = diplomacy_read(s)
        self.assertEqual(i, "D")
        self.assertEqual(j, "Austin")
        self.assertEqual(k, "Move")
        self.assertEqual(l, "London")
        
    def test_read_5(self):
        s = "C London Support B\n"
        i, j, k, l = diplomacy_read(s)
        self.assertEqual(i, "C")
        self.assertEqual(j, "London")
        self.assertEqual(k, "Support")
        self.assertEqual(l, "B")
        
    def test_read_6(self):
        s = "D Paris Support B\n"
        i, j, k, l = diplomacy_read(s)
        self.assertEqual(i, "D")
        self.assertEqual(j, "Paris")
        self.assertEqual(k, "Support")
        self.assertEqual(l, "B")
        
    # ----
    # print
    # ----
    
    """
    these unit tests test the 
    print function in Diplomacy.py
    
    input: a string that contains the army
           name and the army status
    output: a string that combines these
            elements into a single string
            and prints it out
    """
    
    def test_print_1(self):
        w = StringIO()
        diplomacy_print(w, "A", "[Dead]")
        self.assertEqual(w.getvalue(), "A [Dead]\n")
        
    def test_print_2(self):
        w = StringIO()
        diplomacy_print(w, "C", "Madrid")
        self.assertEqual(w.getvalue(), "C Madrid\n")
        
    def test_print_3(self):
        w = StringIO()
        diplomacy_print(w, "B", "London")
        self.assertEqual(w.getvalue(), "B London\n")

    def test_print_4(self):
        w = StringIO()
        diplomacy_print(w, "E", "[Dead]")
        self.assertEqual(w.getvalue(), "E [Dead]\n")
        
    # ----
    # solve
    # ----
    
    """
    these unit tests test the 
    solve function in Diplomacy.py
    
    input: a string that contains
           all the armies and their
           respective elements
    output: a string that will be printed
            with the results of the interactions
            between the armies that include the
            army name and army status
    """

    def test_solve_1(self):
        r = StringIO("A Madrid Hold\nB Barcelona Move Madrid\nC London Support B\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A [dead]\nB Madrid\nC London\n")
            
    def test_solve_2(self):
        r = StringIO("A Madrid Hold\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A Madrid\n")
            
    def test_solve_3(self):
        r = StringIO("A Madrid Hold\nB Barcelona Move Madrid\nC London Move Madrid\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A [dead]\nB [dead]\nC [dead]\n")
            
    def test_solve_4(self):
        r = StringIO("A Madrid Hold\nB Barcelona Move Madrid\nC London Move Madrid\nD Paris Support B\nE Austin Support A")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A [dead]\nB [dead]\nC [dead]\nD Paris\nE Austin\n")
            
    def test_solve_5(self):
        r = StringIO("A Madrid Hold\nB Barcelona Move Madrid\nC London Support B\nD Austin Move London\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A [dead]\nB [dead]\nC [dead]\nD [dead]\n")
            
    def test_solve_6(self):
        r = StringIO("A Madrid Support B\nB Barcelona Move Austin\nC London Move Austin\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A Madrid\nB Austin\nC [dead]\n")

# ----
# main
# ----

if __name__ == "__main__": # pragma: no cover
    main()
