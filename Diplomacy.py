#!/usr/bin/env python3

# ---------------------------
# projects/diplomacy/Diplomacy.py
# ---------------------------


# -----------
# world class
# -----------


class world(object):
    def __init__(self, ):
        self.armies = []                                                  # initiates a list of army objects that we can manipulate

    def add_army(self, s):
        """
        add the army objects into the list
        """
        a = army(*s)                                                      # initiates an army object with given input
        self.armies.append(a)                                             # adds the army object into the list
        
    def sort_by_action(self, action):
        """
        sort armies by a given action
        """
        li = []                                                           # creates an new list to store armies with the same action
        for army in self.armies:                                          # loops through all the army objects to check their actions
            if army.action == action:
                li.append(army)                                           # if the current army fits the given action, add them to the list
        return li                                                         # returns the list

    def allocate_support(self, supporters):
        """
        allocate support from supporting armies
        """
        for supporter in supporters:                                      # loops through all the supporters to check their targets
            for army in self.armies:
                if army.name == supporter.target:                         # if one of the armies fits the described target, adds to its strength
                    army.degree += 1                                      # adds to the variable describing the strength of that army
    
    def sort_by_target(self, target):
        """
        sort armies by a given target
        """
        li = []                                                           # creates an new list to store armies with the same target
        for army in self.armies:                                          # loops through all the army objects to check their actions
            if army.target == target and army.status != "[dead]":
                li.append(army)                                           # adds the army with the same target to the list
        return li                                                         # returns the list
        


# ----------
# army class
# ----------


class army():
    def __init__(self, name, start, action, target):                      # initiates an army object with the following attributes
        self.name = name
        self.start = start
        self.action = action
        self.target = target
        self.degree = 1
        self.status = start


# --------------
# diplomacy_read
# --------------


def diplomacy_read(s):
    """
    read a single line into a list of strings
    s a string
    return a string list of four attributes
    """
    i = s.split()                                                         # stores a single line of input in a list of strings
    if i[2] == "Hold":
        i.append(i[1])                                                    # complements the armies that are holding with a target
    return i                                                              # returns the list of strings


# --------------
# diplomacy_eval
# --------------


def diplomacy_eval(wrld):
    supporters = wrld.sort_by_action("Support")                           # sorts all the supporting armies in a list
    attackers = wrld.sort_by_action("Move")                               # sorts all the moving armies in a list

    for supporter in supporters:
        for attacker in attackers:                                        # loops through the lists to see if the supporter's attacked
            if supporter.start == attacker.target:                        # if the supporter is attacked, forfeits its supporting action
                supporter.action = "Hold"
                supporter.target = supporter.start                        # changes it to the holding position to defend itself
    
    supporters = wrld.sort_by_action("Support")                           # sorts again to allocate the supports from the safe supporters
    wrld.allocate_support(supporters)                                     # add every supporting army's support to the strength of their target

    sorted_attackers = sorted(attackers, key=lambda x: x.target)          # sort the attackers with the same target together
    for i in range(len(sorted_attackers)):                                # loops through the sorted list
        if i != 0 and sorted_attackers[i].target == sorted_attackers[i - 1].target:
            pass                                                          # if an object's evaluated, ignores it
        else:
            battle = wrld.sort_by_target(sorted_attackers[i].target)      # puts armies with the same target together to compare them
            max_degree = 0                                                # stores the value of the greatest strength (degree)
            max_cnt = 1                                                   # stores the number of armies with the greatest strength
            for army in battle:
                if army.degree > max_degree:
                    max_degree = army.degree                              # if the degree of the army currently evaluated is bigger than
                    max_cnt = 1                                           # the previous max, make it the max and set the max count to 1
                elif army.degree == max_degree:                           # if the degree of the army currently evaluated is equal to the
                    max_cnt += 1                                          # previous max, add 1 to the max count variable
            if max_cnt > 1:
                for army in battle:                                       # if multiple armies possess the same biggest strength, everyone
                    army.status = "[dead]"                                # targeting this same city is dead according to the rule
            else:
                for army in battle:
                    if army.degree != max_degree:
                        army.status = "[dead]"                            # if there is a winner, then losers are dead
                    else:
                        army.status = army.target                         # the winner gets what it wants


# ---------------
# diplomacy_print
# ---------------


def diplomacy_print(w, name, status):
    """
    print the end status for each army
    w a writer
    j the list with end status of each army
    """
    
    w.write(name + " " + status + "\n")                                   # prints the name of the army and its final status




# ---------------
# diplomacy_solve
# ---------------


def diplomacy_solve(r, w):
    """
    r a reader
    w a writer
    """
    # initiates the world object serving as the board of the game
    wrld = world()
    
    for s in r:
        # reads the input
        i = diplomacy_read(s)
        # adds the input as an army object
        wrld.add_army(i)

    # evaluates the result of the game
    j = diplomacy_eval(wrld)
    for army in wrld.armies:
        name = army.name
        status = army.status
        # prints the result
        diplomacy_print(w, name, status)